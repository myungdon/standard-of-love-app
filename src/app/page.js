import Image from "next/image";
// import styles from "@/app/page.module.css";

export default function Home() {
  return (
      <main className="flex min-h-screen flex-col items-center justify-between p-24">
        <p className="text-6xl font-serif font-bold text-red-300">연애의 정석</p>
        <div>
          <Image
              src="/main_img.png"
              alt="Next.js Logo"
              width={300}
              height={37}
              priority
          />
        </div>
        <div className="content-center">
          <div className=" grid text-center lg:max-w-5xl lg:grid-cols-1 ">
            <a
                href="http://localhost:3000/pages/story"
                className="group rounded-lg border px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30 "
                rel="noopener noreferrer"
            >
              <h2 className={`mb-1 text-2xl font-semibold`}>
                Start
              </h2>
            </a>
          </div>
        </div>
      </main>
  );
}
