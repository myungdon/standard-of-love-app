// 'use client'
//
// import { useState } from "react";
// import axios from "axios";
//
// export default function Page() {
//     const [question, setQuestion] = useState('')
//     const [response, setResponse] = useState('')
//
//     const handleQuestion = e => {
//         setQuestion(e.target.value)
//     }
//
//     const handleSubmit = e => {
//         e.preventDefault()
//
//         const apiUrl = 'https://generativelanguage.googleapis.com/v1/models/gemini-1.5-flash:generateContent?key=AIzaSyB_P_HcSmI7Qh1fgAUa8YQjE4noRNW2abU'
//
//         const data = { "contents":[{"parts":[{"text": question}]}]}
//         axios.post(apiUrl, data)
//             .then(res => {
//                 setResponse(res.data.candidate[0].content.parts[0].text)
//             })
//             .catch(err => {
//                 console.log(err)
//             })
//     }
//
//     return (
//         <div>
//             <form onSubmit={handleSubmit}>
//                 <input type="text" value={question} onChange={handleQuestion} placeholder="어떤 노래가 듣고 싶어요?"/>
//                 <button type="submit">확인</button>
//             </form>
//             {response && <p>{response}</p>}
//         </div>
//     )
// }
//
// // response && 값이 있을 때 나오고 없을 때 안나온다


'use client'

import { useState } from "react";
import axios from "axios";

export default function Page() {
    const [question, setQuestion] = useState('')
    const [response, setResponse] = useState('')

    const handleQuestion = e => {
        setQuestion(e.target.value)
    }

    const handleSubmit = e => {
        e.preventDefault()

        const apiUrl = 'https://generativelanguage.googleapis.com/v1/models/gemini-1.5-flash:generateContent?key=AIzaSyB_P_HcSmI7Qh1fgAUa8YQjE4noRNW2abU'

        const data = {"contents":[{"parts":[{"text": question}]}]}
        axios.post(apiUrl, data)
            .then(res => {
                setResponse(res.data.candidates[0].content.parts[0].text)
            })
            .catch(err => {
                console.log(err)
            })
    }

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <input type="text" value={question} onChange={handleQuestion} placeholder="어떤 똥이 궁금하세요?" />
                <button type="submit">확인</button>
            </form>
            {response && <p>{response}</p>}
        </div>
    )
}